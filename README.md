# US State

## Overview

This is a Features module that sets up a taxonomy vocabulary for U.S. states and territories. Each term gets some abbreviation data and a geographical region data, via the Geofield module.

## Use case and related module

A possible use case is to associate an entity type with a state, so that users can search for the state that contains their location, or for entities that are within the same state as their location. To accomplish this, the [Geofield Filter Contains](https://www.drupal.org/project/geofield_filter_contains) module my be helpful.

## Installation

Enable the module as normal. The vocabulary and terms will be created.

## After installation

Because this module doesn't include any functionality, you are welcome to disable and uninstall the module immediately. The vocabulary and terms will remain. This will allow you to add the vocabulary to your own site-specific Features module, if desired.
