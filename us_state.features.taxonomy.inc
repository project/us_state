<?php
/**
 * @file
 * us_state.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function us_state_taxonomy_default_vocabularies() {
  return array(
    'us_state' => array(
      'name' => 'U.S. State',
      'machine_name' => 'us_state',
      'description' => 'The 50 states, 5 territories, and 1 federal district of the United States.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
