<?php
/**
 * @file
 * us_state.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function us_state_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'taxonomy_term-us_state-field_us_state_abbreviation'.
  $field_instances['taxonomy_term-us_state-field_us_state_abbreviation'] = array(
    'bundle' => 'us_state',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The abbreviation used for postal addresses.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_us_state_abbreviation',
    'label' => 'Postal abbreviation',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 2,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'taxonomy_term-us_state-field_us_state_geo'.
  $field_instances['taxonomy_term-us_state-field_us_state_geo'] = array(
    'bundle' => 'us_state',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The GeoJSON data describing the boundaries of this state or territory.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_us_state_geo',
    'label' => 'Geographic region',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'geofield',
      'settings' => array(),
      'type' => 'geofield_geojson',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Geographic region');
  t('Postal abbreviation');
  t('The GeoJSON data describing the boundaries of this state or territory.');
  t('The abbreviation used for postal addresses.');

  return $field_instances;
}
