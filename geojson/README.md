# Source of GeoJSON for the U.S. States

The data in this folder was directly taken from [here](https://github.com/johan/world.geo.json/tree/master/countries/USA).

Unfortunately most of the Territories are missing. We need to get their GeoJSON and add them as well.
